#!/bin/sh
# the next line restarts using wish -*- tcl -*- \
exec wish "$0" -- "$@"

# failsafe
after 10000 { exit }

# popup window menu
# create window menu for specified window
# usage: $0 <window id>

# requirement
#   tcl/tk
#   wmctrl
#   xwininfo
#   xkill
#   moveresize-window.tcl

# FIXME: tk_popup uses "grab -global" internally, which causes an error
#        if you are pressing some keys at starting this program.
#        error message :
#          grab failed: another application has grab

proc usage {} {
	global argv0
	puts "usage : $argv0 <window id>"
}

# set window x, y position of specified window id into variable namex, namey
# id : window id
# namex, namey : name of variable set x, y
# return 1 : success
# return 0 : error
proc getWinPos {id namex namey} {
	upvar $namex x
	upvar $namey y
	set fl [open |[list xwininfo -id $id] "r"]
	set data [read $fl]
	if {[catch {close $fl} fid]} {
		# xwininfo error
		# puts $fid
		return 0
	}
	foreach line [split $data \n] {
		if {[string match -nocase "*Absolute upper-left X:*" $line]} {
			set columns [split $line " "]
			set x [lindex $columns end]
		}
		if {[string match -nocase "*Absolute upper-left Y:*" $line]} {
			set columns [split $line " "]
			set y [lindex $columns end]
		}
	}
	return 1
}

# create "Send to" cascade menu
# w : parent menu
# id : window id
proc createSendtoMenu {w id} {
	# send to desktop menu
	set m [menu $w.sendto -tearoff 0]
	# create menu entry for desktop number
	set data [exec wmctrl -d]
	set lines [split $data \n]
	set desk 0
	foreach line $lines {
		$m add command -label $line -underline 0 -command [list exec wmctrl -i -r $id -t $desk]
		incr desk
	}
	$w entryconfigure "Send to" -menu $m
}

proc main {} {
	global argv

	if {![llength $argv] == 1} {
		usage
		exit
	}

	# window id
	set id [lindex $argv 0]

	# get window position
	set x 0
	set y 0
	if {![getWinPos $id x y]} {
		puts "ERROR : cannot get window position"
		puts "        window id : $id"
		exit
	}

	package require Tk

	# create menu
	set m [menu .m -tearoff 0]
	$m add command -label "Move" -underline 0 -command [list exec moveresize-window.tcl move $id]
	$m add command -label "Size" -underline 0 -command [list exec moveresize-window.tcl resize $id]
	$m add command -label "Iconify" -underline 3 -command [list exec wmctrl -i -r $id -b toggle,hidden]
	$m add command -label "Maximize" -underline 2 -command [list exec wmctrl -i -r $id -b toggle,maximized_horz,maximized_vert]
	$m add command -label "Maximize Vertitcal" -underline 9 -command [list exec wmctrl -i -r $id -b toggle,maximized_vert]
	$m add command -label "Maximize Horizontal" -underline 13 -command [list exec wmctrl -i -r $id -b toggle,maximized_horz]
	$m add command -label "Fullscreen" -underline 0 -command [list exec wmctrl -i -r $id -b toggle,fullscreen]
	$m add separator
	$m add command -label "Above" -command [list exec wmctrl -i -r $id -b toggle,above]
	$m add command -label "Below" -command [list exec wmctrl -i -r $id -b toggle,below]
	$m add separator
	$m add command -label "Sticky(A)" -underline 7 -command [list exec wmctrl -i -r $id -b toggle,sticky]
	$m add cascade -label "Send to" -underline 5
	$m add separator
	$m add command -label "Close" -underline 0 -command [list exec wmctrl -i -c $id]
	$m add command -label "Kill" -command [list exec xkill -id $id]
	# exit if menu is disappear
	bind $m <Unmap> {
		# wait for command to execute
		after 100 exit
	}
	# create submenu
	createSendtoMenu $m $id

	# hide default window
	wm geometry . 1x1-1-1
	update
	wm withdraw .

	# pupop menu and cursor position is in first item
	tk_popup $m $x $y 0
}

################################################################
### program start

main
