#!/bin/sh
# the next line restarts using wish \
exec wish "$0" -- "$@"

# window list

# requirement
#   tcl/tk
#   wmctrl

package require Tk

# avoid lost focus when quit program
proc quitProgram {} {
	after 200 {destroy .}
}

# activate window by window id and exit program
proc activateWindowAndExit {id} {
	exec wmctrl -i -a $id
	quitProgram
}

# return window id that selected in treeview
proc getWindowIdfromTree {tree} {
	set focus [$tree focus]
	set itemvalue [$tree item $focus -values]
	return [lindex $itemvalue 0]
}

# activate focused item in treeview
proc selectItem {tree} {
	set id [getWindowIdfromTree $tree]
	# tree item may return ""
	if {$id ne ""} {
		activateWindowAndExit $id
	}
}

# toggle sticky item in treeview
proc stickyWindow {tree} {
	set id [getWindowIdfromTree $tree]
	# tree item may return ""
	if {$id ne ""} {
		exec wmctrl -i -r $id -b toggle,sticky
	}
}

# toggle above item in treeview
proc aboveWindow {tree} {
	set id [getWindowIdfromTree $tree]
	# tree item may return ""
	if {$id ne ""} {
		exec wmctrl -i -r $id -b toggle,above
	}
}

# toggle below item in treeview
proc belowWindow {tree} {
	set id [getWindowIdfromTree $tree]
	# tree item may return ""
	if {$id ne ""} {
		exec wmctrl -i -r $id -b toggle,below
	}
}

# toggle shaded item in treeview
proc ShadedWindow {tree} {
	set id [getWindowIdfromTree $tree]
	# tree item may return ""
	if {$id ne ""} {
		exec wmctrl -i -r $id -b toggle,shaded
	}
}

# toggle skip taskbar item in treeview
proc skipTaskbarWindow {tree} {
	set id [getWindowIdfromTree $tree]
	# tree item may return ""
	if {$id ne ""} {
		exec wmctrl -i -r $id -b toggle,skip_taskbar
	}
}

# toggle skip pager item in treeview
proc skipPagerWindow {tree} {
	set id [getWindowIdfromTree $tree]
	# tree item may return ""
	if {$id ne ""} {
		exec wmctrl -i -r $id -b toggle,skip_pager
	}
}

# close a window by window id
proc closeWindow {id} {
	exec wmctrl -i -c $id
}

# close a focused item in treeview
proc closeItem {tree} {
	set focus [$tree focus]
	set id [getWindowIdfromTree $tree]
	# tree item may return ""
	if {$id ne ""} {
		# before close, get current cursor position
		set pos 0
		foreach row [$tree children {}] {
			if {$row eq $focus} { break }
			incr pos
		}

		# close focused item window
		closeWindow $id

		# remove from treeview
		$tree delete $focus

		# restore cursor position
		setFocus $tree $pos
	}
}

# sort treeview
proc sortBy {tree col direction} {
	set data {}
	foreach row [$tree children {}] {
		lappend data [list [$tree set $row $col] $row]
	}
	set dir [expr {$direction ? "-decreasing" : "-increasing"}]
	set r 0
	foreach info [lsort $dir $data] {
		$tree move [lindex $info 1] {} [incr r]
	}
	$tree heading $col -command [list sortBy $tree $col [expr {!$direction}]]
}

# select title whose first letter matches key
proc handleKeys {tree key state} {
	if {$key eq {}} {
		return
	}
	# do nothing if alt key is pressed
	if {$state eq 8} {
		return
	}

	# escape special character
	switch $key {
		\\ -
		\[ -
		\? -
		\* {set key \\$key}
	}
	set items [$tree children {}]
	set titles {}
	set names [$tree cget -columns]
	set col_title [lsearch $names title]
	foreach item [$tree children {}] {
		lappend titles [lindex [$tree item $item -values] $col_title]
	}
	# search start from next cursor
	set focus [$tree focus]
	set index [$tree index $focus]
	set match [lsearch -nocase -start [expr 1 + $index] $titles $key*]
	# if not match, search again from first item
	if {$match < 0} {
		set match [lsearch -nocase $titles $key*]
	}
	# matched
	if {$match >= 0} {
		setFocus $tree $match
	}
}

# set focus and selection of treeview
proc setFocus {tree index} {
	set children [$tree children {}]

	# focus end of items if index is greater than number of items
	if {[llength $children] <= $index} {
		set index end
	}
	$tree focus [lindex $children $index]
	$tree selection set [lindex $children $index]
}

# set treeview contents with window information
proc setTreeviewItems {tree} {
	# delete old contents
	$tree delete [$tree children {}]
	# get list of windows
	set fl [open "| wmctrl -p -G -l"]
	while {[gets $fl line] >= 0} {

		# split a line into list
		set fields [regexp -all -inline {\S+} $line]

		# string which inserted into the treeview
		set item {}
		# id
		lappend item [lindex $fields 0]
		# desktop
		lappend item [lindex $fields 1]
		# pid
		lappend item [lindex $fields 2]
		# x
		set x [lindex $fields 3]
		# y
		set y [lindex $fields 4]
		# w
		set w [lindex $fields 5]
		# h
		set h [lindex $fields 6]
		# geometry (WxH+X+Y)
		lappend item [string cat $w x $h + $x + $y]
		# clientmachine
		lappend item [lindex $fields 7]
		# title (possibly it contains spaces)
		set title [join [lrange $fields 8 end]]
		lappend item $title
		# insert item into treeview
		$tree insert {} end -values $item
	}

	# sort by desktop number
	sortBy $tree desktop 0

	# select first item in treeview
	focus $tree
	setFocus $tree 0
}

# flash and invoke button widget
proc buttonFlashInvoke {b} {
	$b flash
	$b invoke
}

set programName "WindowList"

# hide startup window
wm withdraw .
# use toplevel as main window
set top [toplevel .winlist]
wm title $top $programName

## treeview
set column_names {id desktop pid geometry clientmachine title}
ttk::treeview $top.tree -height 20 -columns $column_names -show headings

# heading setting
$top.tree heading id -text id -command [list sortBy $top.tree id 0]
$top.tree heading desktop -text desktop -command [list sortBy $top.tree desktop 0]
$top.tree heading pid -text pid -command [list sortBy $top.tree pid 0]
$top.tree heading geometry -text geometry -command [list sortBy $top.tree geometry 0]
$top.tree heading clientmachine -text clientmachine -command [list sortBy $top.tree clientmachine 0]
$top.tree heading title -text title -command [list sortBy $top.tree title 0]
# column setting
$top.tree column id -width 100 -stretch 0
$top.tree column desktop -width 60 -stretch 0
$top.tree column pid -width 50 -stretch 0
$top.tree column geometry -width 160 -stretch 0
$top.tree column clientmachine -width 150 -stretch 0
$top.tree column title -width 300 -stretch 1

# key bindings
bind $top <Key-Escape> {quitProgram}
bind $top.tree <Key-space> "selectItem $top.tree"
bind $top.tree <Key-Return> "selectItem $top.tree"
bind $top.tree <Double-Button-1> "selectItem $top.tree"
bind $top.tree <Key-Home> [list setFocus $top.tree 0]
bind $top.tree <Key-End> [list setFocus $top.tree end]
bind $top.tree <Key> [list handleKeys $top.tree %A %s]
bind $top <Destroy> {quitProgram}

# set treeview items
setTreeviewItems $top.tree

# make window transient
wm attributes $top -type dialog

# pack treeview
pack $top.tree -fill both -expand 1

# buttons in bottom frame
frame $top.bottom
button $top.bottom.close -text "End Task" -underline 0 -command [list closeItem $top.tree]
button $top.bottom.select -text "Switch to" -underline 0 -command [list selectItem $top.tree]
bind $top <Alt-Key-e> [list buttonFlashInvoke $top.bottom.close]
bind $top <Alt-Key-s> [list buttonFlashInvoke $top.bottom.select]
pack $top.bottom.close $top.bottom.select $top.bottom.close -side left
pack $top.bottom -side bottom

# menu
menu $top.menu -tearoff 0
# window menu
set m $top.menu.window
menu $m -tearoff 0
$top.menu add cascade -label "Window" -menu $m -underline 0
$m add command -label "Switch to" -command [list selectItem $top.tree]
$m add command -label "Toggle Sticky" -command [list stickyWindow $top.tree]
$m add command -label "Toggle Above" -command [list aboveWindow $top.tree]
$m add command -label "Toggle Below" -command [list belowWindow $top.tree]
$m add separator
$m add command -label "Toggle Shaded" -command [list ShadedWindow $top.tree]
$m add command -label "Toggle Skip Taskbar" -command [list skipTaskbarWindow $top.tree]
$m add command -label "Toggle Skip Pager" -command [list skipPagerWindow $top.tree]
$m add separator
$m add command -label "Exit $programName" -command quitProgram
$top configure -menu $top.menu
# view menu
set m $top.menu.view
menu $m -tearoff 0
$top.menu add cascade -label "View" -menu $m -underline 0
$m add command -label "Refresh now" -command [list setTreeviewItems $top.tree]

# Local Variables:
# mode: tcl
# End:
