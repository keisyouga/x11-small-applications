#!/bin/sh
# move the active window

# requirement
#   bash or other similar shell
#   wmctrl
#   xdotool
#   awk
#   xwininfo
#   xprop
#   xrandr

### functions

usage () {
	echo "usage :"
	echo "  $0 x y"
	echo "  $0 (left | right | up | down)"
}

# print active window
print_active_window () {
	xdotool getactivewindow
}

# print window ids of current desktop and sticky window
print_window_list () {
	current_desktop=$( wmctrl -d | awk '/\*/ { print $1 }' )
	wmctrl -l -G | awk "\$2 == $current_desktop || \$2 == -1 { print \$1 }"
}

# print window position and frame size
# usage :
#   eval $( get_window_size id )
# note :
#   X1 Y1 X2 Y2 are considered the frame size
get_window_size () {
	# process xwininfo and xprop output together
	{ xwininfo -id $1 ; xprop -id $1 _NET_FRAME_EXTENTS ; } | awk '
# process xwininfo output
/Absolute upper-left X:/ { absx = $4 }
/Absolute upper-left Y:/ { absy = $4 }
/Relative upper-left X:/ { relx = $4 }
/Relative upper-left Y:/ { rely = $4 }
/Width:/ { w = $2 }
/Height:/ { h = $2 }
# process xprop output
/_NET_FRAME_EXTENTS\(CARDINAL\) = / {
gsub(",", "")
frameleft = $3
frameright = $4
frametop = $5
framebottom = $6
}
# print
END {
print "X1=" absx - frameleft
print "Y1=" absy - frametop
print "X2=" absx + w + frameright
print "Y2=" absy + h + framebottom
print "FRAMELEFT=" frameleft
print "FRAMERIGHT=" frameright
print "FRAMETOP=" frametop
print "FRAMEBOTTOM=" framebottom
}'
}

# if move window1 to left|right|up|down, window1 hit window2 ?
# arguments : left|right|up|down w1x1 w1y1 w1x2 w1y2 w2x1 w2y1 w2x2 w2y2
check_hit () {
	direction=$1
	w1x1=$2
	w1y1=$3
	w1x2=$4
	w1y2=$5
	w2x1=$6
	w2y1=$7
	w2x2=$8
	w2y2=$9

	case $direction in
		left)
			[ $(( w1y1 )) -ge $(( w2y2 )) ] && return 1
			[ $(( w1y2 )) -le $(( w2y1 )) ] && return 2
			[ $(( w1x1 )) -le $(( w2x2 )) ] && return 3
			;;
		right)
			[ $(( w1y1 )) -ge $(( w2y2 )) ] && return 1
			[ $(( w1y2 )) -le $(( w2y1 )) ] && return 2
			[ $(( w1x2 )) -ge $(( w2x1 )) ] && return 3
			;;
		up)
			[ $(( w1x1 )) -ge $(( w2x2 )) ] && return 1
			[ $(( w1x2 )) -le $(( w2x1 )) ] && return 2
			[ $(( w1y1 )) -le $(( w2y2 )) ] && return 3
			;;
		down)
			[ $(( w1x1 )) -ge $(( w2x2 )) ] && return 1
			[ $(( w1x2 )) -le $(( w2x1 )) ] && return 2
			[ $(( w1y2 )) -ge $(( w2y1 )) ] && return 3
			;;
	esac

	# hit
	return 0
}

# update variable nearest
# arguments are same as check_hit()
update_nearest () {
	check_hit $@ || return

	direction=$1
	x1=$6
	y1=$7
	x2=$8
	y2=$9
	# update nearest
	case $direction in
		left)
			[ $nearest -lt $(( x2 )) ] && nearest=$(( x2 ))
			;;
		right)
			[ $nearest -gt $(( x1 )) ] && nearest=$(( x1 ))
			;;
		up)
			[ $nearest -lt $(( y2 )) ] && nearest=$(( y2 ))
			;;
		down)
			[ $nearest -gt $(( y1 )) ] && nearest=$(( y1 ))
			;;
	esac
}

# move window
# first parameter is direction : left|right|up|down
# second parameter is target_window
move_direction () {
	[ $# -eq 2 ] || return

	direction=$1
	target_window=$2

	# get size of target window
	eval $( get_window_size $target_window )
	# set top-left and bottom-right position variables of target window
	a_x1=$X1
	a_y1=$Y1
	a_x2=$X2
	a_y2=$Y2

	# default nearest
	case $direction in
		left) nearest=$rootwin_x ;;
		right) nearest=$rootwin_w ;;
		up) nearest=$rootwin_y ;;
		down) nearest=$rootwin_h ;;
	esac

	# find a nearest window to the target window on the left side which y-axis is overlapped
	for win in $( print_window_list )
	do
		eval $( get_window_size $win )
		update_nearest $direction $a_x1 $a_y1 $a_x2 $a_y2 $X1 $Y1 $X2 $Y2
	done

	# hit to workarea border ?
	# update_nearest $direction $a_x1 $a_y1 $a_x2 $a_y2 $workarea_x $workarea_y $workarea_w $workarea_h
	update_nearest $direction $a_x1 $a_y1 $a_x2 $a_y2 $rootwin_x $(( workarea_y + workarea_h )) $rootwin_w $rootwin_y
	update_nearest $direction $a_x1 $a_y1 $a_x2 $a_y2 $rootwin_x $rootwin_y $rootwin_w $workarea_y
	update_nearest $direction $a_x1 $a_y1 $a_x2 $a_y2 $(( workarea_x + workarea_w )) $rootwin_y $rootwin_w $rootwin_h
	update_nearest $direction $a_x1 $a_y1 $a_x2 $a_y2 $rootwin_x $rootwin_y $workarea_x $rootwin_h

	# hit to monitor border ?
	geos=$( xrandr | sed -En '/([0-9]+x[0-9]+\+[0-9]+\+[0-9]+).*$/{s//MARK\1/;s/^.*MARK//p}' )
	for geo in $geos
	do
		display_w=` echo "$geo" | sed 's/x.*$//' `
		display_h=` echo "$geo" | sed 's/^.*x//;s/\+.*$//' `
		display_x=` echo "$geo" | sed 's/^[^+]*+//;s/\+.*$//' `
		display_y=` echo "$geo" | sed 's/^.*+//'`

		X1=$(( display_x ))
		Y1=$(( display_y ))
		X2=$(( display_x + display_w ))
		Y2=$(( display_y + display_h ))

		update_nearest $direction $a_x1 $a_y1 $a_x2 $a_y2 $X1 $Y1 $X2 $Y2 || continue
	done

	# move
	case $direction in
		left)
			wmctrl -i -r $target_window -e 0,$nearest,-1,-1,-1
			;;
		right)
			wmctrl -i -r $target_window -e 0,$(( nearest - (a_x2 - a_x1) )),-1,-1,-1
			;;
		up)
			wmctrl -i -r $target_window -e 0,-1,$nearest,-1,-1
			;;
		down)
			wmctrl -i -r $target_window -e 0,-1,$(( nearest - (a_y2 - a_y1) )),-1,-1
			;;
	esac
}

# move to position
# first parameter is target window
# second parameter is x position
# third parameter is y position
move_to () {
	echo wmctrl -i -r $1 -e 0,$2,$3,-1,-1
	wmctrl -i -r $1 -e 0,$2,$3,-1,-1
}

### program start

# get x , y , w , h of workarea
workarea_xy=$( wmctrl -d | awk '$2 == "*" { print $8 }' )
workarea_x=$( echo $workarea_xy | awk -F, '{ print $1 }' )
workarea_y=$( echo $workarea_xy | awk -F, '{ print $2 }' )
workarea_wh=$( wmctrl -d | awk '$2 == "*" { print $9 }' )
workarea_w=$( echo $workarea_wh | awk -Fx '{ print $1 }' )
workarea_h=$( echo $workarea_wh | awk -Fx '{ print $2 }' )

## TODO: adjust for multiple monitor
## assumes multiples monitor as a separate window
rootwin_x=0
rootwin_y=0
rootwin_wh=$( wmctrl -d | awk '$2 == "*" { print $4 }' )
rootwin_w=$( echo $rootwin_wh | awk -Fx '{ print $1 }' )
rootwin_h=$( echo $rootwin_wh | awk -Fx '{ print $2 }' )

active_window=$( print_active_window )

case $# in
	1)
		# move the window to hit other window
		case "$1" in
			left | right | up | down )
				move_direction $1 $active_window
				;;
			*)
				usage
				exit
				;;
		esac
		;;
	2)
		# move the window to x y position
		move_to $active_window $1 $2
		;;
	*)
		usage
		exit
		;;
esac
