#!/bin/sh
# the next line restarts using wish \
exec wish "$0" -- "$@"

# move or resize (active) window
#
# requirement
#   tcl/tk
#   xdotool
#
# usage :
#   moveresize-window.tcl move [windowid]
#   moveresize-window.tcl resize [windowid]
#
# key bindings
#
#   directions
#
#     y  k  u          7  8  9
#      \ | /            \ | /
#     h- . -l          4- . -6
#      / | \            / | \
#     b  j  n          1  2  3
#
# +---------------+-------------------------------+
# |    r, tab     |      toggle move/resize       |
# +---------------+-------------------------------+
# |   q, escape   | quit, restore window geometry |
# +---------------+-------------------------------+
# | space, return |             quit              |
# +---------------+-------------------------------+

package require Tk

# variables and procedures of target window
namespace eval win {
	variable orig
	variable new
	variable id

	proc init {winid} {
		variable orig
		variable new
		variable id

		set id [format 0x%x $winid]

		array set orig {}
		getWinGeometry $id orig
		# copy from `orig' to `new'
		array set new [array get orig]
	}

	proc moveRelative {dx dy} {
		variable id
		variable new
		incr new(x) $dx
		incr new(y) $dy
		exec xdotool windowmove $id $new(x) $new(y)
	}

	proc resizeRelative {dx dy} {
		variable id
		variable new
		variable resizeLeft
		variable resizeUp

		if {$dx} {
			# if resizeLeft move the window to left
			if {![info exists resizeLeft]} {
				set resizeLeft [expr $dx < 0]
			}
			if {$resizeLeft} {
				incr new(x) $dx
				incr new(w) [expr -$dx]
			} else {
				incr new(w) $dx
			}
		}
		if {$dy} {
			# if resizeUp move the window to up
			if {![info exists resizeUp]} {
				set resizeUp [expr $dy < 0]
			}
			if {$resizeUp} {
				incr new(y) $dy
				incr new(h) [expr -$dy]
			} else {
				incr new(h) $dy
			}
		}
		exec xdotool windowsize $id $new(w) $new(h) windowmove $id $new(x) $new(y)
	}

	proc cancel {} {
		variable id
		variable orig
		exec xdotool windowmove $id $orig(x) $orig(y)
		exec xdotool windowsize $id $orig(w) $orig(h)
	}
}

# take window id , return window geometry in winvar
# note :
#   x and y are frame inclusive
#   w and h are frame exclusive
proc getWinGeometry {id winvar} {
	# get window geometry
	set chan [open [list |xwininfo -id $id]]
	while {[gets $chan line] >= 0} {
		# puts $line
		regexp {Absolute upper-left X: *([-0-9]+)} $line matchvar absx
		regexp {Absolute upper-left Y: *([-0-9]+)} $line matchvar absy
		regexp {Relative upper-left X: *([-0-9]+)} $line matchvar relx
		regexp {Relative upper-left Y: *([-0-9]+)} $line matchvar rely
		regexp {Width: *([0-9]+)} $line matchvar width
		regexp {Height: *([0-9]+)} $line matchvar height
	}
	close $chan

	# get frame size
	set chan [open [list |xprop -id $id]]
	while {[gets $chan line] >= 0} {
		# puts $line
		regexp {_NET_FRAME_EXTENTS\(CARDINAL\) *= *([0-9]+), *([0-9]+), *([0-9]+), *([0-9]+)} \
		    $line matchvar left right top bottom
	}
	close $chan

	# return geometry information in winvar
	upvar $winvar win
	set win(x) [expr $absx - $left]
	set win(y) [expr $absy - $top]
	set win(w) $width
	set win(h) $height
}

# key handler for move mode
proc handleMoveKey {sym state} {
	# puts "sym=$sym state=$state"

	set amount 1
	# shift key is on
	if {$state & 1} {
		set amount [expr $amount * 4]
	}
	# ctrl key is on
	if {$state & 4} {
		set amount [expr $amount * 16]
	}

	switch -nocase $sym {
		KP_8 -
		KP_Up -
		Up -
		k {
			win::moveRelative 0 -$amount
		}
		KP_2 -
		KP_Down -
		Down -
		j {
			win::moveRelative 0 $amount
		}
		KP_4 -
		KP_Left -
		Left -
		h {
			win::moveRelative -$amount 0
		}
		KP_6 -
		KP_Right -
		Right -
		l {
			win::moveRelative $amount 0
		}
		KP_7 -
		KP_Home -
		y {
			win::moveRelative -$amount -$amount
		}
		KP_9 -
		KP_Prior -
		u {
			win::moveRelative $amount -$amount
		}
		KP_1 -
		KP_End -
		b {
			win::moveRelative -$amount $amount
		}
		KP_3 -
		KP_Next -
		n {
			win::moveRelative $amount $amount
		}
		Return -
		Space {
			exit
		}
		Escape -
		q {
			win::cancel
			exit
		}
	}
}

# key handler for resize mode
proc handleResizeKey {sym state} {
	# puts "sym=$sym state=$state"

	set amount 1
	# shift key is on
	if {$state & 1} {
		set amount [expr $amount * 4]
	}
	# ctrl key is on
	if {$state & 4} {
		set amount [expr $amount * 16]
	}

	switch -nocase $sym {
		KP_8 -
		KP_Up -
		Up -
		k {
			win::resizeRelative 0 -$amount
		}
		KP_2 -
		KP_Down -
		Down -
		j {
			win::resizeRelative 0 $amount
		}
		KP_4 -
		KP_Left -
		Left -
		h {
			win::resizeRelative -$amount 0
		}
		KP_6 -
		KP_Right -
		Right -
		l {
			win::resizeRelative $amount 0
		}
		KP_7 -
		KP_Home -
		y {
			win::resizeRelative -$amount -$amount
		}
		KP_9 -
		KP_Prior -
		u {
			win::resizeRelative $amount -$amount
		}
		KP_1 -
		KP_End -
		b {
			win::resizeRelative -$amount $amount
		}
		KP_3 -
		KP_Next -
		n {
			win::resizeRelative $amount $amount
		}
		Return -
		Space {
			exit
		}
		Escape -
		q {
			win::cancel
			exit
		}
	}
}

# resizemode => movemode
proc moveMode {} {
	global mode
	set mode move
	bind . <Key> {handleMoveKey %K %s}
	bind . <Key-r> resizeMode
	bind . <Key-Tab> resizeMode
}

# movemode => resizemode
# reset resizeLeft and resizeUp flag
proc resizeMode {} {
	global mode
	set mode resize
	unset -nocomplain win::resizeLeft
	unset -nocomplain win::resizeUp
	bind . <Key> {handleResizeKey %K %s}
	bind . <Key-r> moveMode
	bind . <Key-Tab> moveMode
}

proc usage {} {
	global argv0
	puts "usage : $argv0 <move|resize> \[windowid\]"
	puts "if windowid is ommited, current active window is used."
}

proc main {} {
	global argv

	wm withdraw .

	# set move or resize
	switch [lindex $argv 0] {
		"move" {
			moveMode
		}
		"resize" {
			resizeMode
		}
		default {
			usage
			exit
		}
	}

	# set window id
	set id [lindex $::argv 1]
	if {$id == ""} {
		# active window
		set id [exec xdotool getactivewindow]
	}
	win::init $id

	# create some widgets
	# window id
	set w [frame .id]
	grid [label $w.lid -text {window id}] -row 0 -column 0
	grid [entry $w.eid -takefocus 0 -width 10 -textvariable win::id] -row 1 -column 0
	pack $w -side left
	# x y width height
	set w [frame .geometry]
	foreach label {x y width height} var {x y w h} col {1 2 3 4} {
		grid [label $w.l$var -text $label] -row 0 -column $col
		grid [entry $w.e$var -takefocus 0 -width 5 -textvariable win::new($var)] -row 1 -column $col
	}
	pack $w -side left
	# mode radiobutton
	set w [frame .mode]
	pack [radiobutton $w.movemode -takefocus 0 -value move -variable mode -text move -command moveMode]
	pack [radiobutton $w.resizemode -takefocus 0 -value resize -variable mode -text resize -command resizeMode]
	pack $w -side left

	wm deiconify .
}

################################################################
### program start
main

# Local Variables:
# mode: tcl
# End:
