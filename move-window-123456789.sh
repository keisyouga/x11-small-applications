#!/bin/sh

# move window

# requirement
#   wmctrl
#   xdotool
#   awk
#   bash or other similar shell

# window position
# 7|8|9
# -+-+-
# 4|5|6
# -+-+-
# 1|2|3

# limitation :
#   do not work for maximized window

# move window
# syntax : move windowid x y
move () {
	wmctrl -i -r $1 -e 0,$2,$3,-1,-1
}

usage () {
	echo "move window to screen corner"
	echo "usage : $0 <1|2|3|4|5|6|7|8|9>"
}

# get x , y , w , h of workarea
workarea_xy=$( wmctrl -d | awk '$2 == "*" { print $8 }' )
workarea_x=$( echo $workarea_xy | awk -F, '{ print $1 }' )
workarea_y=$( echo $workarea_xy | awk -F, '{ print $2 }' )
workarea_wh=$( wmctrl -d | awk '$2 == "*" { print $9 }' )
workarea_w=$( echo $workarea_wh | awk -Fx '{ print $1 }' )
workarea_h=$( echo $workarea_wh | awk -Fx '{ print $2 }' )

# get w , h of target window
win_wh=$( xdotool getactivewindow getwindowgeometry | awk '/Geometry/ { print $2 }' )
win_w=$( echo $win_wh | awk -Fx '{ print $1 }' )
win_h=$( echo $win_wh | awk -Fx '{ print $2 }' )

# get window id of target window
win_id=$( xdotool getactivewindow )

case $1 in
	1)
		x=$(( workarea_x ))
		y=$(( workarea_y + workarea_h - win_h))
		;;
	2)
		x=$(( workarea_x + (workarea_w - win_w) / 2 ))
		y=$(( workarea_y + workarea_h - win_h ))
		;;
	3)
		x=$(( workarea_x + workarea_w - win_w ))
		y=$(( workarea_y + workarea_h - win_h ))
		;;
	4)
		x=$(( workarea_x ))
		y=$(( workarea_y + (workarea_h - win_h) / 2 ))
		;;
	5)
		x=$(( workarea_x + (workarea_w - win_w) / 2 ))
		y=$(( workarea_y + (workarea_h - win_h) / 2 ))
		;;
	6)
		x=$(( workarea_x + workarea_w - win_w ))
		y=$(( workarea_y + (workarea_h - win_h) / 2 ))
		;;
	7)
		x=$(( workarea_x ))
		y=$(( workarea_y ))
		;;
	8)
		x=$(( workarea_x + (workarea_w - win_w) / 2 ))
		y=$(( workarea_y ))
		;;
	9)
		x=$(( workarea_x + workarea_w - win_w ))
		y=$(( workarea_y ))
		;;
	*)
		usage
		exit
		;;
esac

move $win_id $x $y
