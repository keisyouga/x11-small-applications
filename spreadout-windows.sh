#!/bin/sh
# spread out windows

# requirement
#   bash or other similar shell
#   wmctrl
#   awk
#   xdotool
#   xprop
#   grep
#   xrandr
#   xwininfo
#   sed
#   wc

### functions

usage () {
	echo "usage :"
	echo "  $0 layout"
	echo
	echo "following layouts are supported : "
	echo "  even_horizontal"
	echo "  even_vertical"
	echo "  main_horizontal"
	echo "  main_vertical"
	echo "  tiled"
}

# set window size
# syntax : set_window_size winid x y w h
set_window_size () {

	# window width
	w=$4
	# window height
	h=$5

	# subtract frame size from width and height
	# xprop output like this :
	#   _NET_FRAME_EXTENTS(CARDINAL) = left, right, top, bottom
	frame_extents=$( xprop -id $1 | grep '_NET_FRAME_EXTENTS(CARDINAL) = ' )
	if [ -n "$frame_extents" ]
	then
		# get size of window frame into variable
		#   frame_left , frame_right , frame_top , frame_bottom
		eval $( echo $frame_extents | awk '{
gsub(",","")
print "frame_left=" $3
print "frame_right=" $4
print "frame_top=" $5
print "frame_bottom=" $6
}' )
		w=$(( w - frame_left - frame_right ))
		h=$(( h - frame_top - frame_bottom ))
	fi

	# removes maximized flag first
	wmctrl -i -r $1 -b remove,maximized_horz,maximized_vert
	# move and resize window
	wmctrl -i -r $1 -e 0,$2,$3,$w,$h
}

# windows are spread out horizontally
even_horizontal () {
	width=$(( workarea_w / $# ))
	height=$(( workarea_h ))

	x1=$workarea_x
	while [ -n "$1" ]
	do
		set_window_size $1 $x1 $workarea_y $width $height
		shift
		x1=$(( x1 + width ))
	done
}

# windows are spread out vertically
even_vertical () {
	width=$(( workarea_w ))
	height=$(( workarea_h / $# ))

	y1=$workarea_y
	while [ -n "$1" ]
	do
		set_window_size $1 $workarea_x $y1 $width $height
		shift
		y1=$(( y1 + height ))
	done
}

# check $1 included in after $2 as number
is_included () {
	a=$1
	shift
	for i in $@
	do
		if [ $(( a )) -eq $(( i )) ]
		then
			return 0
		fi
	done
	return 1
}

# take window list, print active window first and print rest of window
reorder_active_first () {
	active=$( xdotool getactivewindow )

	if is_included "$active" $@
	then
		# active window is included in window list

		# print active window id
		printf "0x%08x\n" "$active"
		for id in "$@"
		do
			# print rest of window id
			if [ $(( active )) -ne $(( id )) ]
			then
				echo "$id"
			fi

		done
	else
		# if no active window, print argments unchanged
		echo "$@"
	fi
}

# main window occupys top half of screen, other windows shares bottom half of screen
main_horizontal () {
	# if there is only one window, fit to workarea and exit
	if [ $# = 1 ]
	then
		set_window_size $1 $workarea_x $workarea_y $workarea_w $workarea_h
		exit
	fi

	# size of window other than main window
	width=$(( workarea_w / ($# - 1) ))
	height=$(( workarea_h / 2 ))

	# main window
	set_window_size $1 $workarea_x $workarea_y $workarea_w $height
	shift

	# other window
	x1=$(( workarea_x ))
	y1=$(( workarea_y + height ))
	while [ -n "$1" ]
	do
		set_window_size $1 $x1 $y1 $width $height
		shift
		x1=$(( x1 + width ))
	done
}

# main window occupys left half of screen, other windows shares right half of screen
main_vertical () {
	# if there is only one window, fit to workarea and exit
	if [ $# = 1 ]
	then
		set_window_size $1 $workarea_x $workarea_y $workarea_w $workarea_h
		exit
	fi

	# size of window other than main window
	width=$(( workarea_w / 2 ))
	height=$(( workarea_h / ($# - 1) ))

	# main window
	set_window_size $1 $workarea_x $workarea_y $width $workarea_h
	shift

	# other window
	x1=$(( workarea_x + width ))
	y1=$(( workarea_y ))
	while [ -n "$1" ]
	do
		set_window_size $1 $x1 $y1 $width $height
		shift
		y1=$(( y1 + height ))
	done
}

# print number of row from number of window, called in tiled()
# ceil(sqrt(num_window))
get_row () {
	echo $1 | awk '{ n = sqrt($1) ; print(int(n) + (n == int(n) ? 0 : 1)) }'
}

# print number of col from number of window and row, called in tiled()
# ceil(num_window/rows)
get_col () {
	echo $1 $2 | awk '{ n = $1 / $2 ; print(int(n) + (n == int(n) ? 0 : 1)) }'
}

# tiled
# rows = ceil(sqrt(num_window))
# columns = ceil(num_window/rows)
tiled () {
	num_row=` get_row $# `
	num_col=` get_col $# $num_row `
	width=$(( workarea_w / num_col ))
	height=$(( workarea_h / num_row ))
	col=0
	row=0
	while [ -n "$1" ]
	do
		x1=$(( workarea_x + width * col ))
		y1=$(( workarea_y + height * row ))
		if [ $# -eq 1 ]
		then
			# last window
			# enlarge last window to fit screen
			lastwidth=$(( workarea_w - (x1 - workarea_x) ))
			set_window_size $1 $x1 $y1 $lastwidth $height
		else
			# not a last window
			set_window_size $1 $x1 $y1 $width $height
		fi

		col=$(( col + 1 ))
		if [ $col -ge $num_col ]
		then
			# go next row
			col=0
			row=$(( row + 1 ))
		fi

		# next window
		shift
	done
}

# is window minimized ?
is_minimized () {
	xprop -id $1 | grep '_NET_WM_STATE(ATOM) *=.*_NET_WM_STATE_HIDDEN' > /dev/null
}

# take list of windows, print windows that are not minimized
exclude_minimized_window () {
	for id in $@
	do
		is_minimized $id || echo $id
	done
}

# get window id and set it to list_of_windows variable
set_list_of_windows () {
	# find windows in current desktop (-1 is sticky window)
	# list of window id separated by "\n"
	list_of_windows=$( wmctrl -l | awk "\$2 ~ /$current_desktop|-1/ { print \$1 }" )

	# no window is found
	if [ -z "$list_of_windows" ]
	then
		return
	fi

	# exclude minimized window from list
	tmp=` exclude_minimized_window $list_of_windows `
	list_of_windows="$tmp"

	# no non-minimized window is found
	if [ -z "$list_of_windows" ]
	then
		return
	fi

	# list of window id active window first
	tmp=` reorder_active_first $list_of_windows `
	list_of_windows="$tmp"
}

# extract geometry of display from xrandr output
# note : output may contains multiple lines
extract_geometry_of_display () {
	xrandr | sed -En '/([0-9]+x[0-9]+\+[0-9]+\+[0-9]+).*$/{s//MARK\1/;s/^.*MARK//p}'
}

# take window id, print x and y of window position
print_x_y () {
	xwininfo -id $1 | awk '
/Absolute upper-left X:/ { absx = $4 }
/Absolute upper-left Y:/ { absy = $4 }
END { print absx, absy }
'
}

# if has multiple monitors, use xrandr's output instead of workarea property
use_xrandr_geometry () {
	# get display geometries
	geos=` extract_geometry_of_display `

	# no geometry information
	[ -z "$geos" ] && return

	# number of displays
	num_geos=$( echo "$geos" | wc -l | awk '{print$1}' )

	# has multiple monitors ?
	[ $num_geos -ge 2 ] || return

	# get x and y of first window (maybe active window)
	firstwin=$( echo "$list_of_windows" | head -1 )
	winxy=$( print_x_y $firstwin )
	winx=$( echo "$winxy" | awk '{print$1}' )
	winy=$( echo "$winxy" | awk '{print$2}' )

	for geo in $geos
	do

		display_w=` echo "$geo" | sed 's/x.*$//' `
		display_h=` echo "$geo" | sed 's/^.*x//;s/\+.*$//' `
		display_x=` echo "$geo" | sed 's/^[^+]*+//;s/\+.*$//' `
		display_y=` echo "$geo" | sed 's/^.*+//'`

		# examine whether the first window is in the display
		[ $(( winx )) -lt $(( display_x )) ] && continue
		[ $(( winy )) -lt $(( display_y )) ] && continue
		[ $(( winx )) -ge $(( display_x + display_w )) ] && continue
		[ $(( winy )) -ge $(( display_y + display_h )) ] && continue

		# use this geometry instead of workarea property
		workarea_w="$display_w"
		workarea_h="$display_h"
		workarea_x="$display_x"
		workarea_y="$display_y"
	done
}

### program start

# current desktop number
current_desktop=$( wmctrl -d | awk '/\*/ { print $1 }' )

# get x , y , w , h of workarea
workarea_xy=$( wmctrl -d | awk '$2 == "*" { print $8 }' )
workarea_x=$( echo $workarea_xy | awk -F, '{ print $1 }' )
workarea_y=$( echo $workarea_xy | awk -F, '{ print $2 }' )
workarea_wh=$( wmctrl -d | awk '$2 == "*" { print $9 }' )
workarea_w=$( echo $workarea_wh | awk -Fx '{ print $1 }' )
workarea_h=$( echo $workarea_wh | awk -Fx '{ print $2 }' )

# set list_of_windows variable
set_list_of_windows

# do nothing if no window is found
if [ -z "$list_of_windows" ]
then
	exit
fi

# overwrite workarea_x, workarea_y, workarea_w, workarea_h if we have multiple monitors
use_xrandr_geometry

case $1 in
	even_horizontal)
		even_horizontal $list_of_windows
		;;
	even_vertical)
		even_vertical $list_of_windows
		;;
	main_horizontal)
		main_horizontal $list_of_windows
		;;
	main_vertical)
		main_vertical $list_of_windows
		;;
	tiled)
		tiled $list_of_windows
		;;
	*)
		usage
		;;
esac
